#!/bin/sh
case $1 in
    period-changed)
        exec notify-send "Redshift" "Colour changed to $3"
esac
