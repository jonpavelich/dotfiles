set nocompatible        " options not compatible with vi

syntax enable           " syntax highlighting
set showmatch           " highlight matching [{()}]

filetype indent on      " filetype specific indenting
filetype plugin on      " filetype specific plugins
set autoindent

set history=500         " remember 500 lines of command history

set number              " lines numbers
set relativenumber      " numbers above and below current line are relative

set tabstop=4           " number of visual spaces per tab
set softtabstop=4       " number of spaces per tab when editing
set expandtab           " tabs are spaces

set autoindent

set showcmd             " show command in bar
set cursorline          " highlight current line
set wildmenu            " autocomplete for command manu
set lazyredraw          " redraw only when we need to

set autoread            " autoread outside changes to file-s

let mapleader = ","     " map leader for extra key combinations
let g:mapleader = ","

set incsearch           " search as characters are entered
set hlsearch            " highlight search results, noh to mute

set foldenable          " enable folding
set foldlevelstart=10   " open most folds
set foldnestmax=10      " limit nested folds to 10 layers
set foldmethod=syntax

" use different spacing for YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" move to beginning/end of line
nnoremap B ^
nnoremap E $

" unmap the old ones to train - can be removed once used to new ones
nnoremap ^ <nop>
nnoremap $ <nop>

" jk is escape
inoremap jk <esc>

" fast save
nmap <leader>w :w!<cr>

" sudo save
command W w !sudo tee % > /dev/null

" open/close folds
nnoremap <space> za

" mute search highlighting
nnoremap <leader><space> :noh<cr>

" remember the following things on exit:
"   marks for the last 10 files
"   100 lines for each register
"   20 lines of command line history
set viminfo='10,\"100,:20,%,n~/.viminfo

" Return to last edit position when opening file
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
