# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
export PATH="$HOME/.local/bin:$HOME/.bin:$PATH"

# Use gpgagent for SSH (for Yubikey)
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null 2>&1

# Keep space-prefixed commands out of history and logs
# Erase duplicate commands in-memory (not on disk), saving only the latest invocation
# We don't ignoredups, because the history timestamp needs to update to log repeated commands properly
HISTCONTROL=ignorespace:erasedups
shopt -s cmdhist
shopt -s histappend

preexec_log_command() {
    _last_cmd_timestamped=$_this_cmd_timestamped
    _this_cmd_timestamped=$(HISTTIMEFORMAT='%s ' history 1)

    # Log all commands to a file
    # - commands starting with a leading space are ignored
    # - a repeated command is logged only once per second
    
    # echo "DEBUG: _last_cmd_timestamped is ($_last_cmd_timestamped)"
    # echo "DEBUG: _this_cmd_timestamped is ($_this_cmd_timestamped)"


    local _cmd
    if [[ "$_this_cmd_timestamped" != "$_last_cmd_timestamped" ]]; then
        printf '%s [%7d] %s  \t%s\n' "$(date "+%FT%T%z")" "$$" "$(pwd)" "$@" >> ~/.logs/bash/history-$(date "+%F").log
    fi
}

preexec_functions+=(preexec_log_command)

precmd_append_history() {
    history -a
}

precmd_functions+=(precmd_append_history)

# Aliases and functions
function check-variable {
    if [[ "${!1}" == "" ]]; then
        echo "Error: variable $1 is not defined (try exporting it in ~/.bashrc.local)"
        return -1
    fi
    return 0
}

function vpn {
    if [ "$1" == "up" ]; then
        sudo protonvpn-cli -cc ca
    elif [ "$1" == "down" ]; then
        sudo protonvpn-cli -d
    else
        sudo protonvpn-cli $@
    fi
}

function open {
    xdg-open $@ 1>/dev/null 2>&1 &
    disown
}

function nosleep {
    while [ 1 -eq 1 ]; do
        echo -ne "    \r"
        xdotool key F13
        echo -ne "\rStaying awake (Ctrl-C to exit)..."
        sleep 59
    done
}

function rate-fortune {
    echo "$FORTUNE"
    read -p "Do you like this fortune? [y/n]: " answer
    if [ "$answer" == "y" ]; then
        echo -ne "$FORTUNE\n=+=+=\n" >> "$HOME/.fortunes/like.txt"
    elif [ "$answer" == "n" ]; then
        echo -ne "$FORTUNE\n=+=+=\n" >> "$HOME/.fortunes/dislike.txt"
    fi
}

function hdmi-video-on {
    if ! check-variable HDMI_DISPLAY; then return; fi
    if ! check-variable MAIN_DISPLAY; then return; fi

    if [ "$1" == "" ] || [ "$1" == "right" ]; then
        xrandr --output $HDMI_DISPLAY --auto --right-of $MAIN_DISPLAY
    elif [ "$1" == "left" ]; then
        xrandr --output $HDMI_DISPLAY --auto --left-of $MAIN_DISPLAY
    else
        echo "Supported directions: left, right"
    fi
}

function hdmi-video-off {
    if ! check-variable HDMI_DISPLAY; then return; fi
    xrandr --output $HDMI_DISPLAY --off
}

function hdmi-audio {
    pulseaudio -k
}


function git-authors {
    echo "Summarizing line counts on branch $(git rev-parse --abbrev-ref HEAD) ($(git rev-parse --short HEAD))"
    git ls-tree -r HEAD | sed -re 's/^.{53}//' | while read filename; do file "$filename"; done | grep -E ': .*text' | sed -r -e 's/: .*//' | while read filename; do git blame -w "$filename"; done | sed -r -e 's/.*\((.*)[0-9]{4}-[0-9]{2}-[0-9]{2} .*/\1/' -e 's/ +$//' | sort | uniq -c | sort -nbr
}

function random {
    dd if=/dev/urandom bs=16 count=1 2>/dev/null | xxd -ps
}

alias vote='rate-fortune'
alias sl='ls'
alias dict='sdcv'

# Ansible shouldn't show cows because its harder to read
export ANSIBLE_NOCOWS=1

# Machine specific config
if [ -f $HOME/.bashrc.local ]; then
    source $HOME/.bashrc.local
fi

# Extras for interactive shells
if [[ $- == *i* ]]; then
    # Silly banner on each new terminal
    if command -v fortune >/dev/null && command -v cowsay >/dev/null; then
        #COWS="bud-frogs bunny cower default elephant flaming-sheep head-in kitty koala luke-koala meow moofasa moose mutilated sheep skeleton small three-eyes tux udder vader vader-koala www"
        COWS="bunny default kitty moose small tux"
        FORTUNE=$(fortune -s)
        BANNER=$(echo -n "$FORTUNE" | cowthink -f $(echo -e ${COWS//\ /\\n} | shuf -n1))
        export FORTUNE  # export for function rate-fortune
        echo -ne "$FORTUNE\n=+=+=\n" >> "$HOME/.fortunes/displayed.txt"
        if command -v lolcat >/dev/null; then
            echo "$BANNER" | lolcat
        else
            echo "$BANNER"
            echo "This is more fun in colour, install lolcat!"
        fi
    fi
    
    if command -v i3 >/dev/null; then
        # Activate notify-when-done-i3 which notifies when commands finish in other workspaces
        # Requires bash-preexec
        if [ -f ~/.nwd/preexec.sh ]; then
            source ~/.nwd/preexec.sh
        fi
    fi
fi


## THIS SECTION MUST BE LAST

# Activate bash-preexec
if [ -f ~/.bash-preexec.sh ]; then
        source ~/.bash-preexec.sh
fi

# Override built in function from bash-preexec
function __bp_adjust_histcontrol {
    true
}
