#!/bin/bash
set -e

echo "This script is only designed for Fedora."

read -p "Upgrade system? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf upgrade
fi

read -p "Install RPM Fusion? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf check-update

    read -p "Install ffmpeg? [y/N] " CHOICE
    if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
        sudo dnf install ffmpeg 
    fi
fi

read -p "Install essentials group? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install firefox git irssi light pass the_silver_searcher redshift restic
fi

read -p "Install i3 group? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install i3 i3status i3lock rxvt-unicode-256color dmenu rofi feh xautolock
    read -p "Include i3blocks? [y/N] " CHOICE
    if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
        sudo dnf install autoconf automake gcc sysstat
        sudo git clone https://github.com/vivien/i3blocks /opt/i3blocks
        PREV="$(pwd)"
        cd /opt/i3blocks
        sudo ./autogen.sh
        sudo ./configure
        sudo make
        sudo make install
        cd $PREV
    fi
fi

read -p "Install extras group? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install cowsay ffsend figlet fortune-mod lolcat syncthing
fi

read -p "Install libreoffice? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install libreoffice
fi

read -p "Install snapd? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    sudo dnf install snapd
fi

read -p "Fetch dotfiles? [y/N] " CHOICE
if [ "$CHOICE" == "y" ] || [ "$CHOICE" == "Y" ]; then
    if [ -e "$HOME/.dotfiles" ]; then
        echo "~/.dotfiles already exists, skipping."
    else
        git clone https://gitlab.com/jonpavelich/dotfiles.git $HOME/.dotfiles
        PREV="$(pwd)"
        cd $HOME/.dotfiles
        git remote remove origin
        git remote add origin git@gitlab.com:jonpavelich/dotfiles.git
        echo "Cloned dotfiles to ~/.dotfiles, you need to symlink the ones you want."
        cd $PREV
    fi
fi

echo "All done!"
